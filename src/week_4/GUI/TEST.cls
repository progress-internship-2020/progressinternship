 
 /*------------------------------------------------------------------------
    File        : TEST
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : pDiac
    Created     : Wed Nov 11 13:31:32 EET 2020
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_4.GUI.TEST INHERITS Form: 
    
    DEFINE PRIVATE VARIABLE components AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE textBox2 AS System.Windows.Forms.TextBox NO-UNDO.
    DEFINE PRIVATE VARIABLE textBoxArray1 AS Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray NO-UNDO.
    DEFINE PRIVATE VARIABLE textBox1 AS System.Windows.Forms.TextBox NO-UNDO.

    CONSTRUCTOR PUBLIC TEST (  ):
        
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        THIS-OBJECT:textBoxArray1 = NEW Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray(THIS-OBJECT:components).
        THIS-OBJECT:textBox1 = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textBox2 = NEW System.Windows.Forms.TextBox().
        CAST(THIS-OBJECT:textBoxArray1, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* textBox1 */
        /*  */
        THIS-OBJECT:textBox1:Location = NEW System.Drawing.Point(100, 169).
        THIS-OBJECT:textBox1:Name = "textBox1".
        THIS-OBJECT:textBox1:Size = NEW System.Drawing.Size(100, 22).
        THIS-OBJECT:textBox1:TabIndex = 0.
        THIS-OBJECT:textBox1:TextChanged:Subscribe(THIS-OBJECT:textBox1_TextChanged).
        THIS-OBJECT:textBox1:Leave:Subscribe(THIS-OBJECT:textBox1_Leave).
        /*  */
        /* textBox2 */
        /*  */
        THIS-OBJECT:textBox2:Location = NEW System.Drawing.Point(341, 140).
        THIS-OBJECT:textBox2:Name = "textBox2".
        THIS-OBJECT:textBox2:Size = NEW System.Drawing.Size(100, 22).
        THIS-OBJECT:textBox2:TabIndex = 1.
        /*  */
        /* TEST */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(508, 334).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textBox2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textBox1).
        THIS-OBJECT:Name = "TEST".
        THIS-OBJECT:Text = "TEST".
        CAST(THIS-OBJECT:textBoxArray1, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.


	/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
	@VisualDesigner.
	METHOD PRIVATE VOID textBox1_Leave( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		MESSAGE textBox1:Text
        VIEW-AS ALERT-BOX.
        

	END METHOD.

	/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
	@VisualDesigner.
	METHOD PRIVATE VOID textBox1_TextChanged( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		

	END METHOD.

    DESTRUCTOR PUBLIC TEST ( ):

    END DESTRUCTOR.

END CLASS.