 
 /*------------------------------------------------------------------------
    File        : dataAccess
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : pDiac
    Created     : Tue Nov 10 02:55:12 EET 2020
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.week_3.OOPclasses.ex1.Customers.
USING src.week_4.Ex1.iDataAccess.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_4.Ex1.dataAccess  IMPLEMENTS iDataAccess: 

    METHOD PUBLIC VOID FetchData( OUTPUT Dataset-handle DataHandle ):
    END METHOD.

    METHOD PUBLIC VOID StoreData( INPUT-OUTPUT Dataset-handle DataHandle ):
    END METHOD.

END CLASS.