 
/*------------------------------------------------------------------------
   File        : Customers
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Tue Nov 10 02:56:20 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.week_4.Ex1.dataAccess.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_4.Ex1.Customers INHERITS dataAccess: 
    {D:\progressinternship2020\src\week_4\Ex1\dsCustomer.i}
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    CONSTRUCTOR PUBLIC Customers (  ):
        SUPER ().
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD OVERRIDE PUBLIC VOID FetchData(  OUTPUT Dataset-handle DataHandle ):
        
        getCustomer(INPUT-OUTPUT dataset-handle DataHandle BY-REFERENCE).

    END METHOD.

    METHOD PUBLIC VOID getCustomer(INPUT-OUTPUT dataset dsCustomer):
        FOR EACH customer NO-LOCK:
            CREATE ttCustomer.
            BUFFER-COPY customer TO ttCustomer.
            RELEASE ttCustomer.
        END.
        DATASET dsCustomer:WRITE-XML("file","d:\tmp\0123dsCust3123.xml",TRUE).
    END METHOD.
    
    
    
    METHOD OVERRIDE PUBLIC VOID StoreData(INPUT-OUTPUT Dataset-handle DataHandle):
        
    END METHOD.

END CLASS.