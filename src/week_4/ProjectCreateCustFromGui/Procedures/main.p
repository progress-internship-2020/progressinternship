
/*------------------------------------------------------------------------
    File        : main.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : pDiac
    Created     : Wed Nov 11 15:20:44 EET 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.
USING Progress.Lang.*.
USING src.week_4.ProjectCreateCustFromGui.Classes.Customer FROM PROPATH.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */


DEFINE VARIABLE customersInstance AS Customer.

customersInstance = NEW Customer().

customersInstance:CreateCustomerFromGUI("Dedededede"). 
customersInstance:CreateCustomerFromGUI("Dededed1311111111111111ede"). 
customersInstance:CreateCustomerFromGUI("Dedededede123123"). 
customersInstance:CreateCustomerFromGUI("Dedeasddasdasdede"). 
customersInstance:ShowLastCustNum().



