 


USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_4.ProjectCreateCustFromGui.Classes.Customer : 
    {D:\progressinternship2020\src\week_4\ProjectCreateCustFromGui\DataSets\dsCustomer.i}
    DEFINE PRIVATE VARIABLE lastCustNum AS INTEGER.

    CONSTRUCTOR PUBLIC Customer (  ):
        
        
       
    END CONSTRUCTOR.

    

    METHOD PUBLIC VOID CreateCustomerFromGUI( INPUT GUIname AS CHARACTER,  GUIcity AS CHARACTER ):
        
        CREATE ttCustomer.
        ASSIGN
            ttCustomer.CustNum = lastCustNum + 5
            ttCustomer.Name    = GUIname
            ttCustomer.City = GUIcity.
                
        RELEASE  ttCustomer.
        
        lastCustNum = lastCustNum + 5.
        
        DATASET dsCustomer:WRITE-XML("file","D:\progressinternship2020\src\week_4\ProjectCreateCustFromGui\___Validate\check.xml",TRUE).   
        

    END METHOD.



    

    METHOD PUBLIC VOID DeleteCustomerwithCustNum( INPUT CustNumToDelete AS INTEGER):
        
        FIND FIRST ttCustomer WHERE CustNum = CustNumToDelete EXCLUSIVE-LOCK.
        DELETE ttCustomer.
        lastCustNum = lastCustNum - 5.
        DATASET dsCustomer:WRITE-XML("file","D:\progressinternship2020\src\week_4\ProjectCreateCustFromGui\___Validate\check.xml",TRUE).   
    END METHOD.

    
    

    METHOD PUBLIC VOID DeleteLastCustomer(  ):
        
        FIND FIRST ttCustomer WHERE CustNum = lastCustNum EXCLUSIVE-LOCK.
        DELETE ttCustomer.
        lastCustNum = lastCustNum - 5.
        DATASET dsCustomer:WRITE-XML("file","D:\progressinternship2020\src\week_4\ProjectCreateCustFromGui\___Validate\check.xml",TRUE).

    END METHOD.

    
    


    METHOD PUBLIC VOID fillTTCustomer(  ):
        
       FOR EACH Customer NO-LOCK BY Customer.CustNum :
            CREATE ttCustomer.
            BUFFER-COPY Customer TO ttCustomer.
            lastCustNum = ttCustomer.CustNum.
        END.

    END METHOD.

    METHOD PUBLIC VOID ShowLastCustNum(  ):
        
        MESSAGE lastCustNum
            VIEW-AS ALERT-BOX.
    END METHOD.    



    DESTRUCTOR PUBLIC Customer ( ):

    END DESTRUCTOR.

END CLASS.