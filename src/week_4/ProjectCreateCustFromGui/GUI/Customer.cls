 
/*------------------------------------------------------------------------
   File        : Customer
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Wed Nov 11 19:11:52 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.week_4.Ex1.Customers FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_4.ProjectCreateCustFromGui.GUI.Customer INHERITS Form: 
    {D:\progressinternship2020\src\week_4\Ex1\dsCustomer.i}
    DEFINE PRIVATE VARIABLE balanceDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE addressDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE address2DataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE bindingSource1                        AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE button1                               AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE commentsDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE cityDataGridViewTextBoxColumn         AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE components                            AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE custNumDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE creditLimitDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE countryDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE contactDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                         AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE faxDataGridViewTextBoxColumn          AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE emailAddressDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE discountDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE termsDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE stateDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE salesRepDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE postalCodeDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE phoneDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE nameDataGridViewTextBoxColumn         AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    
    
    DEFINE         VARIABLE clsex1Cust                            AS Customers.
    

    CONSTRUCTOR PUBLIC Customer (  ):
        
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        clsex1Cust = NEW Customers().
        clsex1Cust:FetchData(OUTPUT DATASET dsCustomer BY-REFERENCE).
        bindingSource1:Handle = DATASET dsCustomer:HANDLE.
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
        
        
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID button1_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
       
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_CellContentClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.DataGridViewCellEventArgs ):
		
        RETURN.

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc1 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc1 = NEW Progress.Data.TableDesc("ttCustomer").
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:addressDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:address2DataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:balanceDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:cityDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:commentsDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:contactDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:countryDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:creditLimitDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:custNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:discountDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:emailAddressDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:faxDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:nameDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:phoneDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:postalCodeDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:salesRepDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:stateDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:termsDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:bindingSource1 = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:button1 = NEW System.Windows.Forms.Button().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:bindingSource1, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS System.Windows.Forms.DataGridViewColumn EXTENT 18 NO-UNDO.
        arrayvar0[1] = THIS-OBJECT:addressDataGridViewTextBoxColumn.
        arrayvar0[2] = THIS-OBJECT:address2DataGridViewTextBoxColumn.
        arrayvar0[3] = THIS-OBJECT:balanceDataGridViewTextBoxColumn.
        arrayvar0[4] = THIS-OBJECT:cityDataGridViewTextBoxColumn.
        arrayvar0[5] = THIS-OBJECT:commentsDataGridViewTextBoxColumn.
        arrayvar0[6] = THIS-OBJECT:contactDataGridViewTextBoxColumn.
        arrayvar0[7] = THIS-OBJECT:countryDataGridViewTextBoxColumn.
        arrayvar0[8] = THIS-OBJECT:creditLimitDataGridViewTextBoxColumn.
        arrayvar0[9] = THIS-OBJECT:custNumDataGridViewTextBoxColumn.
        arrayvar0[10] = THIS-OBJECT:discountDataGridViewTextBoxColumn.
        arrayvar0[11] = THIS-OBJECT:emailAddressDataGridViewTextBoxColumn.
        arrayvar0[12] = THIS-OBJECT:faxDataGridViewTextBoxColumn.
        arrayvar0[13] = THIS-OBJECT:nameDataGridViewTextBoxColumn.
        arrayvar0[14] = THIS-OBJECT:phoneDataGridViewTextBoxColumn.
        arrayvar0[15] = THIS-OBJECT:postalCodeDataGridViewTextBoxColumn.
        arrayvar0[16] = THIS-OBJECT:salesRepDataGridViewTextBoxColumn.
        arrayvar0[17] = THIS-OBJECT:stateDataGridViewTextBoxColumn.
        arrayvar0[18] = THIS-OBJECT:termsDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar0).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bindingSource1.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(26, 28).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(1016, 467).
        THIS-OBJECT:dataGridView1:TabIndex = 0.
        THIS-OBJECT:dataGridView1:CellContentClick:Subscribe(THIS-OBJECT:dataGridView1_CellContentClick).
        /*  */
        /* addressDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:addressDataGridViewTextBoxColumn:DataPropertyName = "Address".
        THIS-OBJECT:addressDataGridViewTextBoxColumn:HeaderText = "Address".
        THIS-OBJECT:addressDataGridViewTextBoxColumn:Name = "addressDataGridViewTextBoxColumn".
        /*  */
        /* address2DataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:address2DataGridViewTextBoxColumn:DataPropertyName = "Address2".
        THIS-OBJECT:address2DataGridViewTextBoxColumn:HeaderText = "Address2".
        THIS-OBJECT:address2DataGridViewTextBoxColumn:Name = "address2DataGridViewTextBoxColumn".
        /*  */
        /* balanceDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:balanceDataGridViewTextBoxColumn:DataPropertyName = "Balance".
        THIS-OBJECT:balanceDataGridViewTextBoxColumn:HeaderText = "Balance".
        THIS-OBJECT:balanceDataGridViewTextBoxColumn:Name = "balanceDataGridViewTextBoxColumn".
        /*  */
        /* cityDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:cityDataGridViewTextBoxColumn:DataPropertyName = "City".
        THIS-OBJECT:cityDataGridViewTextBoxColumn:HeaderText = "City".
        THIS-OBJECT:cityDataGridViewTextBoxColumn:Name = "cityDataGridViewTextBoxColumn".
        /*  */
        /* commentsDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:commentsDataGridViewTextBoxColumn:DataPropertyName = "Comments".
        THIS-OBJECT:commentsDataGridViewTextBoxColumn:HeaderText = "Comments".
        THIS-OBJECT:commentsDataGridViewTextBoxColumn:Name = "commentsDataGridViewTextBoxColumn".
        /*  */
        /* contactDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:contactDataGridViewTextBoxColumn:DataPropertyName = "Contact".
        THIS-OBJECT:contactDataGridViewTextBoxColumn:HeaderText = "Contact".
        THIS-OBJECT:contactDataGridViewTextBoxColumn:Name = "contactDataGridViewTextBoxColumn".
        /*  */
        /* countryDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:countryDataGridViewTextBoxColumn:DataPropertyName = "Country".
        THIS-OBJECT:countryDataGridViewTextBoxColumn:HeaderText = "Country".
        THIS-OBJECT:countryDataGridViewTextBoxColumn:Name = "countryDataGridViewTextBoxColumn".
        /*  */
        /* creditLimitDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:creditLimitDataGridViewTextBoxColumn:DataPropertyName = "CreditLimit".
        THIS-OBJECT:creditLimitDataGridViewTextBoxColumn:HeaderText = "Credit Limit".
        THIS-OBJECT:creditLimitDataGridViewTextBoxColumn:Name = "creditLimitDataGridViewTextBoxColumn".
        /*  */
        /* custNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:DataPropertyName = "CustNum".
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:HeaderText = "Cust Num".
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:Name = "custNumDataGridViewTextBoxColumn".
        /*  */
        /* discountDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:discountDataGridViewTextBoxColumn:DataPropertyName = "Discount".
        THIS-OBJECT:discountDataGridViewTextBoxColumn:HeaderText = "Discount".
        THIS-OBJECT:discountDataGridViewTextBoxColumn:Name = "discountDataGridViewTextBoxColumn".
        /*  */
        /* emailAddressDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:emailAddressDataGridViewTextBoxColumn:DataPropertyName = "EmailAddress".
        THIS-OBJECT:emailAddressDataGridViewTextBoxColumn:HeaderText = "Email".
        THIS-OBJECT:emailAddressDataGridViewTextBoxColumn:Name = "emailAddressDataGridViewTextBoxColumn".
        /*  */
        /* faxDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:faxDataGridViewTextBoxColumn:DataPropertyName = "Fax".
        THIS-OBJECT:faxDataGridViewTextBoxColumn:HeaderText = "Fax".
        THIS-OBJECT:faxDataGridViewTextBoxColumn:Name = "faxDataGridViewTextBoxColumn".
        /*  */
        /* nameDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:nameDataGridViewTextBoxColumn:DataPropertyName = "Name".
        THIS-OBJECT:nameDataGridViewTextBoxColumn:HeaderText = "Name".
        THIS-OBJECT:nameDataGridViewTextBoxColumn:Name = "nameDataGridViewTextBoxColumn".
        /*  */
        /* phoneDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:phoneDataGridViewTextBoxColumn:DataPropertyName = "Phone".
        THIS-OBJECT:phoneDataGridViewTextBoxColumn:HeaderText = "Phone".
        THIS-OBJECT:phoneDataGridViewTextBoxColumn:Name = "phoneDataGridViewTextBoxColumn".
        /*  */
        /* postalCodeDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:postalCodeDataGridViewTextBoxColumn:DataPropertyName = "PostalCode".
        THIS-OBJECT:postalCodeDataGridViewTextBoxColumn:HeaderText = "Postal Code".
        THIS-OBJECT:postalCodeDataGridViewTextBoxColumn:Name = "postalCodeDataGridViewTextBoxColumn".
        /*  */
        /* salesRepDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:salesRepDataGridViewTextBoxColumn:DataPropertyName = "SalesRep".
        THIS-OBJECT:salesRepDataGridViewTextBoxColumn:HeaderText = "Sales Rep".
        THIS-OBJECT:salesRepDataGridViewTextBoxColumn:Name = "salesRepDataGridViewTextBoxColumn".
        /*  */
        /* stateDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:stateDataGridViewTextBoxColumn:DataPropertyName = "State".
        THIS-OBJECT:stateDataGridViewTextBoxColumn:HeaderText = "State".
        THIS-OBJECT:stateDataGridViewTextBoxColumn:Name = "stateDataGridViewTextBoxColumn".
        /*  */
        /* termsDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:termsDataGridViewTextBoxColumn:DataPropertyName = "Terms".
        THIS-OBJECT:termsDataGridViewTextBoxColumn:HeaderText = "Terms".
        THIS-OBJECT:termsDataGridViewTextBoxColumn:Name = "termsDataGridViewTextBoxColumn".
        /*  */
        /* bindingSource1 */
        /*  */
        THIS-OBJECT:bindingSource1:MaxDataGuess = 0.
        THIS-OBJECT:bindingSource1:NoLOBs = FALSE.
        THIS-OBJECT:bindingSource1:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar1 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc1:ChildTables = arrayvar1.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS Progress.Data.ColumnPropDesc EXTENT 18 NO-UNDO.
        arrayvar2[1] = NEW Progress.Data.ColumnPropDesc("Address", "Address", Progress.Data.DataType:CHARACTER).
        arrayvar2[2] = NEW Progress.Data.ColumnPropDesc("Address2", "Address2", Progress.Data.DataType:CHARACTER).
        arrayvar2[3] = NEW Progress.Data.ColumnPropDesc("Balance", "Balance", Progress.Data.DataType:DECIMAL).
        arrayvar2[4] = NEW Progress.Data.ColumnPropDesc("City", "City", Progress.Data.DataType:CHARACTER).
        arrayvar2[5] = NEW Progress.Data.ColumnPropDesc("Comments", "Comments", Progress.Data.DataType:CHARACTER).
        arrayvar2[6] = NEW Progress.Data.ColumnPropDesc("Contact", "Contact", Progress.Data.DataType:CHARACTER).
        arrayvar2[7] = NEW Progress.Data.ColumnPropDesc("Country", "Country", Progress.Data.DataType:CHARACTER).
        arrayvar2[8] = NEW Progress.Data.ColumnPropDesc("CreditLimit", "Credit Limit", Progress.Data.DataType:DECIMAL).
        arrayvar2[9] = NEW Progress.Data.ColumnPropDesc("CustNum", "Cust Num", Progress.Data.DataType:INTEGER).
        arrayvar2[10] = NEW Progress.Data.ColumnPropDesc("Discount", "Discount", Progress.Data.DataType:INTEGER).
        arrayvar2[11] = NEW Progress.Data.ColumnPropDesc("EmailAddress", "Email", Progress.Data.DataType:CHARACTER).
        arrayvar2[12] = NEW Progress.Data.ColumnPropDesc("Fax", "Fax", Progress.Data.DataType:CHARACTER).
        arrayvar2[13] = NEW Progress.Data.ColumnPropDesc("Name", "Name", Progress.Data.DataType:CHARACTER).
        arrayvar2[14] = NEW Progress.Data.ColumnPropDesc("Phone", "Phone", Progress.Data.DataType:CHARACTER).
        arrayvar2[15] = NEW Progress.Data.ColumnPropDesc("PostalCode", "Postal Code", Progress.Data.DataType:CHARACTER).
        arrayvar2[16] = NEW Progress.Data.ColumnPropDesc("SalesRep", "Sales Rep", Progress.Data.DataType:CHARACTER).
        arrayvar2[17] = NEW Progress.Data.ColumnPropDesc("State", "State", Progress.Data.DataType:CHARACTER).
        arrayvar2[18] = NEW Progress.Data.ColumnPropDesc("Terms", "Terms", Progress.Data.DataType:CHARACTER).
        tableDesc1:Columns = arrayvar2.
        THIS-OBJECT:bindingSource1:TableSchema = tableDesc1.
        /*  */
        /* button1 */
        /*  */
        THIS-OBJECT:button1:Location = NEW System.Drawing.Point(134, 518).
        THIS-OBJECT:button1:Name = "button1".
        THIS-OBJECT:button1:Size = NEW System.Drawing.Size(75, 23).
        THIS-OBJECT:button1:TabIndex = 1.
        THIS-OBJECT:button1:Text = "button1".
        THIS-OBJECT:button1:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:button1:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:button1:Click:Subscribe(THIS-OBJECT:button1_Click).
        /*  */
        /* Customer */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(1116, 553).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:button1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "Customer".
        THIS-OBJECT:Text = "Customer".
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:bindingSource1, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC Customer ( ):

    END DESTRUCTOR.

END CLASS.