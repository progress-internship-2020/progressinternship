 
/*------------------------------------------------------------------------
   File        : Gui_CRUD
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Wed Nov 11 18:48:48 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.week_4.ProjectCreateCustFromGui.Classes.Customer FROM PROPATH.



BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_4.ProjectCreateCustFromGui.GUI.Gui_CRUD INHERITS Form: 
    {D:\progressinternship2020\src\week_4\ProjectCreateCustFromGui\DataSets\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnCreate   AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components  AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE         VARIABLE clsCustomer AS Customer                         NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCity     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblName     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE txtCity     AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE txtName     AS System.Windows.Forms.TextBox     NO-UNDO.
    
    
    CONSTRUCTOR PUBLIC Gui_CRUD (  ):
        
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnCreate_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        DEFINE VARIABLE custcity AS CHARACTER NO-UNDO.
        DEFINE VARIABLE custname AS CHARACTER NO-UNDO.
        ASSIGN
            custcity = txtCity:text
            custname = txtName:text.
        
            clsCustomer = NEW Customer().
            
            clsCustomer:CreateCustomerFromGUI(custname,custcity).

    END METHOD.


    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:lblName = NEW System.Windows.Forms.Label().
        THIS-OBJECT:txtName = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:txtCity = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblCity = NEW System.Windows.Forms.Label().
        THIS-OBJECT:btnCreate = NEW System.Windows.Forms.Button().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* lblName */
        /*  */
        THIS-OBJECT:lblName:Location = NEW System.Drawing.Point(12, 9).
        THIS-OBJECT:lblName:Name = "lblName".
        THIS-OBJECT:lblName:Size = NEW System.Drawing.Size(42, 13).
        THIS-OBJECT:lblName:TabIndex = 1.
        THIS-OBJECT:lblName:Text = "Name:".
        THIS-OBJECT:lblName:UseCompatibleTextRendering = TRUE.
        /*  */
        /* txtName */
        /*  */
        THIS-OBJECT:txtName:Location = NEW System.Drawing.Point(60, 6).
        THIS-OBJECT:txtName:Name = "txtName".
        THIS-OBJECT:txtName:Size = NEW System.Drawing.Size(100, 20).
        THIS-OBJECT:txtName:TabIndex = 2.
        THIS-OBJECT:txtName:TextChanged:Subscribe(THIS-OBJECT:textBox1_TextChanged).
        /*  */
        /* txtCity */
        /*  */
        THIS-OBJECT:txtCity:Location = NEW System.Drawing.Point(60, 32).
        THIS-OBJECT:txtCity:Name = "txtCity".
        THIS-OBJECT:txtCity:Size = NEW System.Drawing.Size(100, 20).
        THIS-OBJECT:txtCity:TabIndex = 4.
        /*  */
        /* lblCity */
        /*  */
        THIS-OBJECT:lblCity:Location = NEW System.Drawing.Point(12, 35).
        THIS-OBJECT:lblCity:Name = "lblCity".
        THIS-OBJECT:lblCity:Size = NEW System.Drawing.Size(42, 13).
        THIS-OBJECT:lblCity:TabIndex = 3.
        THIS-OBJECT:lblCity:Text = "City:".
        THIS-OBJECT:lblCity:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblCity:Click:Subscribe(THIS-OBJECT:label1_Click).
        /*  */
        /* btnCreate */
        /*  */
        THIS-OBJECT:btnCreate:Location = NEW System.Drawing.Point(60, 220).
        THIS-OBJECT:btnCreate:Name = "btnCreate".
        THIS-OBJECT:btnCreate:Size = NEW System.Drawing.Size(75, 23).
        THIS-OBJECT:btnCreate:TabIndex = 5.
        THIS-OBJECT:btnCreate:Text = "Create".
        THIS-OBJECT:btnCreate:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnCreate:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnCreate:Click:Subscribe(THIS-OBJECT:btnCreate_Click).
        /*  */
        /* Gui_CRUD */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(292, 266).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnCreate).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtCity).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCity).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtName).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblName).
        THIS-OBJECT:Name = "Gui_CRUD".
        THIS-OBJECT:Text = "Gui_CRUD".
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID label1_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID textBox1_TextChanged( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC Gui_CRUD ( ):

    END DESTRUCTOR.

END CLASS.