
/*------------------------------------------------------------------------
    File        : functions.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : pDiac
    Created     : Sat Oct 31 15:00:42 EET 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.

USING System.Boolean FROM ASSEMBLY.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */

DEFINE VARIABLE param1 AS INTEGER NO-UNDO.
DEFINE VARIABLE param2 AS INTEGER NO-UNDO.

FUNCTION greater RETURN INTEGER (INPUT param1 AS INTEGER, INPUT param2 AS INTEGER):
    IF param1 > param2 THEN
        RETURN param1.
    ELSE RETURN param2.
    
END FUNCTION.


/*SET param1.                    */
/*SET param2.                    */
/*                               */
/*MESSAGE greater(param1, param2)*/
/*    VIEW-AS ALERT-BOX.         */
/*                               */


FUNCTION checkorder RETURN LOGICAL (INPUT customnumber AS INTEGER):
    if (customnumber = ?) or
       (customnumber = 0) then
       message "customer number is not valid"
       view-as alert-box.
    FIND FIRST Customer WHERE Customer.CustNum = customnumber no-lock NO-ERROR.
    IF ERROR-STATUS:ERROR = TRUE THEN 
    DO:
        MESSAGE "customer with " customnumber " does not exist"
            VIEW-AS ALERT-BOX.
        LEAVE.
    END.
    FIND FIRST order WHERE Order.CustNum = customnumber.
    
    IF AVAILABLE order THEN 
    DO:
        RETURN TRUE.
    END.
    RETURN FALSE.
END FUNCTION.


etime(yes).
MESSAGE checkorder(1)
    VIEW-AS ALERT-BOX.

message "Time:" etime "ms"
  view-as alert-box.

etime(no).
FUNCTION checkorderv2 RETURN LOGICAL (INPUT customnumber AS INTEGER):
  if (customnumber = ?) or
     (customnumber = 0) then
     message "customer number is not valid"
     view-as alert-box.
  for first customer where Customer.CustNum = customnumber no-lock,
    first order where Order.CustNum = Customer.CustNum no-lock:
      return true.
  end.
  return false.
END FUNCTION.


etime(yes).

MESSAGE checkorderv2(1)
    VIEW-AS ALERT-BOX.

message "Time:" etime "ms"
  view-as alert-box.

etime(no).


FUNCTION stringex RETURN CHARACTER (INPUT str AS CHARACTER) :
    
    
    RETURN SUBSTRING(str,1, 3).

END FUNCTION.

message stringex("Alecsandra")
view-as alert-box.


DEFINE VARIABLE i AS INTEGER NO-UNDO.

FUNCTION vowel RETURN INTEGER(INPUT str AS CHARACTER):
  
    i=0.
    
    IF str EQ "" THEN 
    DO:
        RETURN 0.
    END.
    IF SUBSTRING(str,1,1) = "a" 
        OR substring(str,1,1) = "e" 
        OR substring(str,1,1) = "i" 
        OR substring(str,1,1) = "o" 
        OR substring(str,1,1) = "u"
        THEN 
    DO:
        i = i + 1.
        
    END.
   
    
    RETURN i + vowel(SUBSTRING (str,2,LENGTH (str))).

END FUNCTION.

MESSAGE vowel("maine")

    VIEW-AS ALERT-BOX.
    
