
/*------------------------------------------------------------------------
    File        : errorHandling.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : pDiac
    Created     : Fri Oct 30 03:12:14 EET 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */

/*FIND FIRST Customer WHERE CustNum = 1000 NO-ERROR.*/
/*                                                  */
/*IF ERROR-STATUS:ERROR = TRUE THEN                 */
/*    MESSAGE ERROR-STATUS:GET-MESSAGE(1).          */
/*FIND FIRST Customer WHERE CustNum = 1000 NO-ERROR.*/
/*                                                  */
/*IF ERROR-STATUS:ERROR = TRUE THEN                 */
/*    MESSAGE ERROR-STATUS:GET-MESSAGE(1).          */
/*                                                  */



/*                                                                      */
/*PROCEDURE Customer1000:                                               */
/*                                                                      */
/*    FIND FIRST Customer WHERE CustNum = 1000 NO-ERROR.                */
/*                                                                      */
/*    IF ERROR-STATUS:ERROR = TRUE THEN                                 */
/*        RETURN ERROR "Customer 1000 does not exist in this database.".*/
/*                                                                      */
/*END PROCEDURE.                                                        */
/*                                                                      */
/*RUN Customer1000 NO-ERROR.                                            */
/*                                                                      */
/*                                                                      */
/*                                                                      */
/*                                                                      */
/*                                                                      */
/*IF ERROR-STATUS:ERROR = TRUE THEN                                     */
/*    DISPLAY RETURN-VALUE FORMAT "x(60)".                              */
/*ELSE DISPLAY "Procedure completed successfully." FORMAT "x(60)".      */


/*PROCEDURE NestedBlocks:                                 */
/*                                                        */
/*    Outer-Block:                                        */
/*    FOR EACH Customer WHERE CustNum < 3025:             */
/*        ASSIGN                                          */
/*            Customer.Name = Customer.Name + "_changed". */
/*                                                        */
/*        Inner-Block:                                    */
/*        FOR EACH Order OF Customer                      */
/*            ON ERROR UNDO Outer-Block, RETURN ERROR:    */
/*                                                        */
/*            DISPLAY OrderNum.                           */
/*                                                        */
/*            /* Nonsense code raises ERROR. */           */
/*            FIND SalesRep WHERE RepName = Customer.Name.*/
/*        END.                                            */
/*    END.                                                */
/*                                                        */
/*    DISPLAY "For Blocks Complete".                      */
/*END PROCEDURE.                                          */
/*                                                        */
/*RUN NestedBlocks NO-ERROR.                              */
/*                                                        */
/*DISPLAY "Procedure NestedBlocks Complete."              */

/*                                                                    */
/*FIND Customer WHERE CustNum = 9876 NO-ERROR.                        */
/*IF ERROR-STATUS:ERROR = TRUE THEN                                   */
/*    MESSAGE "So sorry, but this Customer does not seem to be there."*/
/*        VIEW-AS ALERT-BOX INFORMATION.                              */
/*                                                                    */
/*                                                                    */



DEFINE VARIABLE iMsg AS INTEGER NO-UNDO.
FIND Customer WHERE customer.CustNum = 9876 NO-ERROR.
IF ERROR-STATUS:ERROR THEN
DO iMsg = 1 TO ERROR-STATUS:NUM-MESSAGES:
    MESSAGE "Error number: " ERROR-STATUS:GET-NUMBER(iMsg) SKIP
        ERROR-STATUS:GET-MESSAGE(iMsg)
        VIEW-AS ALERT-BOX ERROR.
END.