DEFINE TEMP-TABLE ttEmployee NO-UNDO 
    FIELD EmpNumber   AS INTEGER
    FIELD FirstName   AS CHARACTER
    FIELD LastName    AS CHARACTER
    FIELD hasBenefits AS LOGICAL
    INDEX index1 IS PRIMARY UNIQUE EmpNumber.
    
    
DEFINE TEMP-TABLE ttEmployeeWVacantion NO-UNDO 
    FIELD EmpNumber    AS INTEGER
    FIELD FirstName    AS CHARACTER
    FIELD LastName     AS CHARACTER
    FIELD hasVacantion AS LOGICAL
    INDEX index1 IS PRIMARY UNIQUE EmpNumber.
    
DEFINE TEMP-TABLE ttEmployeeHC NO-UNDO 
    FIELD EmpNumber         AS INTEGER
    FIELD FirstName         AS CHARACTER
    FIELD healthCareAtAetna AS LOGICAL
    INDEX index1 IS PRIMARY UNIQUE EmpNumber.