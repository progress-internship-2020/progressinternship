DEFINE TEMP-TABLE ttCustomer NO-UNDO 
    FIELD CustNumber    AS INTEGER
    FIELD CustName      AS CHARACTER
    FIELD City          AS CHARACTER
    FIELD NumberOfOders AS INTEGER
    INDEX index1 IS PRIMARY UNIQUE CustNumber.
  
  
DEFINE TEMP-TABLE ttCustomerUSA NO-UNDO 
    FIELD CustNumber AS INTEGER
    FIELD CustName   AS CHARACTER
    FIELD City       AS CHARACTER
    FIELD Country    AS CHARACTER
  
    INDEX index1 IS PRIMARY UNIQUE CustNumber.
  
DEFINE TEMP-TABLE ttCustomerVAC NO-UNDO 
    FIELD CustNumber   AS INTEGER
    FIELD CustName     AS CHARACTER
    FIELD hasVacantion AS LOGICAL 
  
    INDEX index1 IS PRIMARY UNIQUE CustNumber.
  