 
/*------------------------------------------------------------------------
   File        : Customers
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Fri Nov 06 13:20:01 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.
CLASS src.week_3.OOPclasses.example.Customers: 

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    DEFINE PUBLIC PROPERTY numCustomers AS INTEGER NO-UNDO 
        GET.
        PRIVATE SET. 

    {D:\progressinternship2020\src\week_3\OOPclasses\example\ttcustomer.i &ClassAccess = "private"}

    CONSTRUCTOR PUBLIC Customers (  ):
        SUPER ().
        FOR EACH customer NO-LOCK:
            CREATE ttCustomer.
            ASSIGN 
                ttCustomer.CustNum  = Customer.CustNum
                ttCustomer.Name = Customer.Name
                ttCustomer.City     = Customer.City.
        
            RELEASE ttCustomer. 
            numCustomers = numCustomers + 1.
        END.
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getCustomer(INPUT pCustNum AS INTEGER):
        FIND ttCustomer WHERE ttCustomer.CustNum = pCustNum NO-ERROR.
        IF AVAILABLE ttCustomer THEN
        DO : 
            DISPLAY ttCustomer.
        END.
        RETURN.

    END METHOD.    

    DESTRUCTOR PUBLIC Customers ( ):

    END DESTRUCTOR.

END CLASS.