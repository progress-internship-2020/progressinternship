 
/*------------------------------------------------------------------------
   File        : Customers
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Fri Nov 06 13:20:01 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING OpenEdge.Core.Assert FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.
CLASS src.week_3.OOPclasses.ex1.Customers: 

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    DEFINE PUBLIC PROPERTY numCustomers AS INTEGER NO-UNDO 
        GET.
        PRIVATE SET. 

    {D:\progressinternship2020\src\week_3\OOPclasses\ex1\ttcustomer.i &ClassAccess = "private"}

    CONSTRUCTOR PUBLIC Customers (  ):
        SUPER ().
        FOR EACH customer NO-LOCK:
            CREATE ttCustomer.
            BUFFER-COPY Customer TO ttCustomer.
            RELEASE ttCustomer. 
            numCustomers = numCustomers + 1.
        END.
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getCustomer(INPUT pCustNum AS INTEGER):
        FIND ttCustomer WHERE ttCustomer.CustNum = pCustNum NO-ERROR.
        IF AVAILABLE ttCustomer THEN
        DO : 
            MESSAGE ttCustomer.Name
                VIEW-AS ALERT-BOX.
        END.
        RETURN.

    END METHOD. 

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getCustomerDetails( INPUT pCustNum AS INTEGER ):
        DEFINE VARIABLE TempDir AS CHARACTER NO-UNDO.
        TempDir      = SESSION:TEMP-DIRECTORY.
        OUTPUT to value(SUBSTITUTE("&1_&2", TempDir, "ttCustomer.txt")) append.
        
        FIND ttCustomer WHERE ttCustomer.CustNum = pCustNum NO-LOCK NO-ERROR.
        IF AVAILABLE ttCustomer THEN 
        DO :
            PUT "CustNum : ".
            IF ttCustomer.CustNum = ?  THEN 
            DO:
                PUT "Not defined" SKIP.
            END.
            ELSE PUT ttCustomer.CustNum SKIP.
            
            PUT "Name : ".
            IF ttCustomer.name = ? OR ttCustomer.name = "" THEN 
            DO:
                PUT "Not defined" SKIP.
            END.
            ELSE PUT ttCustomer.name SKIP.
            
            PUT "Address : ".
            IF ttCustomer.Address = ? OR ttCustomer.Address = "" THEN 
            DO:
                PUT "Not defined" SKIP.
            END.
            ELSE PUT ttCustomer.Address SKIP.
            
            PUT "Address2 : ".
            IF ttCustomer.Address2 = ? OR ttCustomer.Address2 = "" THEN 
            DO:
                PUT "Not defined" SKIP.
            END.
            ELSE PUT ttCustomer.Address2 SKIP.
            
            PUT "Balance : ".
            IF ttCustomer.balance = ?  THEN 
            DO:
                PUT "Not defined" SKIP.
            END.
            ELSE PUT ttCustomer.balance SKIP.
            
            
            PUT "City : ".
            IF ttCustomer.city = ? OR ttCustomer.city = "" THEN 
            DO:
                PUT "Not defined" SKIP.
            END.
            ELSE PUT ttCustomer.city SKIP.
            
        END.
        
        OUTPUT close.
        RETURN.

    END METHOD.    

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC LOGICAL hasOrders(INPUT customerNumber AS INTEGER):
        

        RETURN CAN-FIND (FIRST Order 
        WHERE Order.CustNum = customerNumber NO-LOCK ).

    END METHOD.    
    
    
    
       

    DESTRUCTOR PUBLIC Customers ( ):

    END DESTRUCTOR.

END CLASS.




