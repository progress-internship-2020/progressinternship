 
/*------------------------------------------------------------------------
   File        : Orders
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Sat Nov 07 16:00:01 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING OpenEdge.CORE.Assert FROM PROPATH.
BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.week_3.OOPclasses.ex2.Orders: 

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    {D:\progressinternship2020\src\week_3\OOPclasses\ex2\ttOrder.i}
    CONSTRUCTOR PUBLIC Orders (  ):
        FOR EACH order NO-LOCK:
            CREATE ttOrder.
            BUFFER-COPY  order TO ttOrder.
        END.
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getOrderForCustomer(INPUT customerNumber AS INTEGER):
        ASSERT:notnull(customerNumber).
        assert:NotZero(customerNumber).
        FOR EACH ttOrder WHERE ttOrder.CustNum = customerNumber NO-LOCK:
            DISPLAY ttOrder.
        END.
        
        RETURN.

    END METHOD.



    DESTRUCTOR PUBLIC Orders ( ):

    END DESTRUCTOR.

END CLASS.